#ifndef _BOB_H_
#define _BOB_H_

#include <string>
class InputMemoryStream;
class OutputMemoryStream;

class Bob
{
private:
	int x, y, z;
	float b, c;

	std::string name;
public:
	Bob();
	Bob(int inX, int inY, int inZ, float inB, float inC, std::string inName);
	~Bob();

	void setX(int x);
	int getX();

	void setY(int y);
	int getY();

	void setZ(int z);
	int getZ(); 


	void setB(float b);
	float getB(void);

	void setC(float c);
	float getC(void);

	void setName(std::string name);
	std::string getName();

	bool operator==(Bob& other);

	void Read(InputMemoryStream& in);
	void Write(OutputMemoryStream& out);

};
#endif //_BOB_H_
