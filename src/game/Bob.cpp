#include "Bob.h"

#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"

#include <cmath>

Bob::Bob()
    :x(0),
     y(0),
     z(0),
    b(0.0f),
    c(0.0f),
    name("")
{

}

Bob::Bob(int inX, int inY, int inZ, float inB, float inC, std::string inName)
    :x(inX),
    y(inY),
    z(inZ),
    b(inB),
    c(inC),
    name(inName)
{

}

Bob::~Bob()
{

}

void Bob::setX(int x)
{
    this->x = x;
}

int Bob::getX()
{
    return this->y;
}

void Bob::setY(int y)
{
    this->y = y;
}

int Bob::getY()
{
    return this->y;
}

void Bob::setZ(int z)
{
    this->z = z;
}

int Bob::getZ()
{
    return this->z;
}


void Bob::setB(float b)
{
    this->b = b;
}

float Bob::getB(void)
{
    return this->b;
}

void Bob::setC(float c)
{
    this->c = c;
}

float Bob::getC(void)
{
    return this->c;
}


void Bob::setName(std::string name)
{
    this->name = name;
}

std::string Bob::getName()
{
    return this->name;
}

bool Bob::operator==(Bob& other)
{
    if (x != other.x) return false;
    if (std::abs(b - other.b) > 0.00001f) return false;
    if (!(name == other.name)) return false;

    return true;
}

void Bob::Read(InputMemoryStream& in)
{
    int len = 0;
    char temp = 0;

    in.Read(x);
    in.Read(b);
    in.Read(len);

    name.clear();

    for (int i = 0; i < len; i++)
    {
        in.Read(temp);
        name += temp;
    }

}

void Bob::Write(OutputMemoryStream& out)
{
    out.Write(x);
    out.Write(b);

    int len = name.length();
    out.Write(len);

    for (char& c : name) //if this throws an error your compiler can't handle C++11!
        out.Write(c);

}
